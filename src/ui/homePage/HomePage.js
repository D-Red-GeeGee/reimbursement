import React from 'react';
import Header from './header/Header.Component.js';
import MainContent from './mainContent/_components/MainContent.Component.js';
import Footer from './footer/Footer.Component.js';

export class HomePage extends React.Component {

    render() {

        return (
            <div className="container">
                <div className="row"><Header /></div>
                <div className="row"><MainContent /></div>
                <div className="row"><Footer /></div>
            </div>
        );
    }
}
