import React, { Component } from 'react';
import { userService } from '../../_services/user.services.js';
import { Link } from 'react-router-dom';

class Header extends React.Component {
   constructor(props) {
      super(props);

      this.state = {
         user: {},
         users: []
      };
   }

   componentDidMount() {
      this.setState({
         user: JSON.parse(localStorage.getItem('user')),
         users: { loading: true }
      });
      userService.getAll().then(users => this.setState({ users }));
   }
   render() {
      const { user, users } = this.state;
      return (
         <div className="container">
            <div className="row">
               <div className="col">
                  <h2>Reimbursement Page</h2>
               </div>
               <div className="col">
                  <p>{user.firstName + ' ' + user.lastName} <span><Link to="/login">Logout</Link></span></p>
               </div>
            </div>


         </div>
      );
   }
}
export default Header;