import config from 'config';
import { authHeader } from '../../../_helpers/auth-header.js';

export const MainContentServices = {
    getReimbursements,
    getReimbursementById,
    addNewReimbursement,
    updateReimbursement,
    removeReimbursement
};

function getReimbursements() {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };

    return fetch(`${config.apiUrl}/reimbursements`, requestOptions).then(handleResponse);
}
function getReimbursementById(id) {
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ id })
    };

    return fetch(`${config.apiUrl}/reimbursements/getReimbursementById`, requestOptions)
        .then(handleResponse);
}

function addNewReimbursement(data) {
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(data)
    };

    return fetch(`${config.apiUrl}/reimbursements/addNewReimbursement`, requestOptions)
        .then(handleResponse);
}

function updateReimbursement(data) {
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(data)
    };

    return fetch(`${config.apiUrl}/reimbursements/updateReimbursement`, requestOptions)
        .then(handleResponse);
}

function removeReimbursement(data) {
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(data)
    };

    return fetch(`${config.apiUrl}/reimbursements/removeReimbursement`, requestOptions)
        .then(handleResponse);
}


function handleResponse(response) {
    return response.text().then(text => {
        const data = text && JSON.parse(text);
        if (!response.ok) {
            if (response.status === 401) {
                logout();
                location.reload(true);
            }

            const error = (data && data.message) || response.statusText;
            return Promise.reject(error);
        }

        return data;
    });
}