import React, { Component } from 'react';
import { Modal } from 'react-bootstrap';
import { Button } from 'react-bootstrap';
import { Form, FormControl, FormFile, FormGroup, FormLabel } from 'react-bootstrap';
import { MainContentServices } from '../_services/mainContent.services.js';
import store from "../../../../data_store/index.js";


const monthNames = ["January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"
];

class AddModalDialog extends React.Component {


  constructor() {
    super();


    this.state = {
      showHide: false,
      month: "",
      documents: [],
      status: "",
      amountClaimed: 0,
      parent: "",
      comments: "",
      isValidated: "false"
    }
    this.handleModalShowHide = this.handleModalShowHide.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.setValidate = this.setValidate.bind(this);
  }
  handleModalShowHide() {
    this.setState({ showHide: !this.state.showHide });
  }
  setValidate(status) {
    setState({ 'isValidated': status });
  }
  handleSubmit(type) {

    console.log('submiting...'+type);
    

    var data = {
      month: this.state.month,
      documents: this.state.documents,
      status: this.state.status,
      amountClaimed: this.state.amountClaimed,
      parent: type
    }

    console.log(data);

    MainContentServices.addNewReimbursement(data)
      .then(
        result => {
          MainContentServices.getReimbursements()
            .then(
              reimbursements => {
                store.dispatch(updateReimbursements(reimbursements));
                this.handleModalShowHide();
              },
              error => console.log("Error while fetching data.")
            );
        },
        error => console.log("Error while adding data.")
      );

  }
  componentDidMount() {
    
    this.setState({ parent: this.props.reimType });
    this.setState({ month: monthNames[new Date().getMonth()] });
    this.setState({ documents: [] });
    this.setState({ status: "Open" });
    this.setState({ amountClaimed: 0 });
    this.setState({ comments: "" });
  }

  render() {

    return (
      <div>
        <Button variant="primary" onClick={() => this.handleModalShowHide()}>
          Add New Claim
                </Button>

        <Modal show={this.state.showHide} backdrop="static"
          keyboard={false}>
          <Form>
            <Modal.Header closeButton onClick={() => this.handleModalShowHide()}>
              <Modal.Title>Add New Claim Details</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <Form.Group controlId="parent">
                <Form.Label>Reimbursement Type</Form.Label>
                <Form.Control as="select" required defaultValue={this.props.reimType} onChange={e => this.setState({ parent: e.target.value })} placeholder="Select reimbursement type" >
                  <option value="5f170d5b358869777ba6e409">Reimbursement type 1</option>
                  <option value="5f170deb358869777ba6e40a">Reimbursement type 2</option>
                </Form.Control>
              </Form.Group>

              <Form.Group controlId="month">
                <Form.Label>Month</Form.Label>
                <Form.Control as="select" required defaultValue={monthNames[new Date().getMonth()]} onChange={e => this.setState({ month: e.target.value })} placeholder="Select Month" >
                  <option>January</option>
                  <option>February</option>
                  <option>March</option>
                  <option>April</option>
                  <option>May</option>
                  <option>June</option>
                  <option>July</option>
                  <option>August</option>
                  <option>September</option>
                  <option>October</option>
                  <option>November</option>
                  <option>December</option>
                </Form.Control>
              </Form.Group>
              <Form.Group controlId="amountClaimed">
                <Form.Label>&#8377;Amount</Form.Label>
                <Form.Control type="number" onChange={e => this.setState({ amountClaimed: e.target.value })} placeholder="Enter Amount" />
              </Form.Group>
              <Form.Group controlId="status">
                <Form.Label>Status</Form.Label>
                <Form.Control as="select" defaultValue="Open" required onChange={e => this.setState({ status: e.target.value })} placeholder="Select status" >
                  <option>Open</option>
                  <option>Approved</option>
                  <option>Pending</option>
                  <option>Rejected</option>

                </Form.Control>

              </Form.Group>
              <Form.File id="formcheck-api-custom" custom>
                <Form.File.Input isValid />
                <Form.File.Label data-browse="Browse">
                  Select File(s)
              </Form.File.Label>
                <Form.Control.Feedback type="valid"></Form.Control.Feedback>
              </Form.File>

              <Form.Group controlId="comments">
                <Form.Label>Comments</Form.Label>
                <Form.Control type="textarea" onChange={e => this.setState({ comments: e.target.value })} placeholder="Enter comments..." />
              </Form.Group>

            </Modal.Body>
            <Modal.Footer>
              <Button variant="secondary" onClick={() => this.handleModalShowHide()}>
                Close
                    </Button>
              <Button variant="primary" onClick={() => this.handleSubmit(this.props.reimType)}>
                Save Changes
                    </Button>
            </Modal.Footer>
          </Form>
        </Modal>

      </div >
    )
  }

}

export default AddModalDialog;