import React, { Component } from 'react';
import { Modal } from 'react-bootstrap';
import { Button } from 'react-bootstrap';
import { Form, FormControl, FormFile, FormGroup, FormLabel } from 'react-bootstrap';
import { MainContentServices } from '../_services/mainContent.services.js';
import store from "../../../../data_store/index.js";

class EditModalDialog extends React.Component {

  constructor() {
    super();

    this.state = {
      _id: undefined,
      showHide: false,
      month: undefined,
      documents: [],
      status: undefined,
      amountClaimed: undefined,
      comments: undefined
    }
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleModalShowHide = this.handleModalShowHide.bind(this);
  }
  handleModalShowHide() {
    this.setState({ showHide: !this.state.showHide });
  }
  handleSubmit(event) {
    var data = {
      _id: this.state._id,
      month: this.state.month,
      documents: this.state.documents,
      status: this.state.status,
      amountClaimed: this.state.amountClaimed,
    }
    MainContentServices.updateReimbursement(data)
      .then(
        result => {
          MainContentServices.getReimbursements()
          .then(
            reimbursements => {
              store.dispatch(updateReimbursements(reimbursements));
              this.handleModalShowHide();
            },
            error => console.log("Error while fetching data.")
          );
          
        },
        error => console.log("Error while updating data.")
      );
   
  }
  componentDidMount() {
    this.setState({ _id: this.props.rowData._id });
    this.setState({ month: this.props.rowData.month });
    this.setState({ documents: this.props.rowData.documents });
    this.setState({ status: this.props.rowData.status });
    this.setState({ amountClaimed: this.props.rowData.amountClaimed });
    this.setState({ comments: this.props.rowData.comments });
  }

  render() {

    return (
      <div>
        <Button variant="primary" onClick={() => this.handleModalShowHide()}>
          Edit
                </Button>
          <Modal show={this.state.showHide} backdrop="static"
            keyboard={false}>
             <Form> 
            <Modal.Header closeButton onClick={() => this.handleModalShowHide()}>
              <Modal.Title>Edit Claim Details</Modal.Title>
            </Modal.Header>
            <Modal.Body>

              <Form.Group controlId="month">
                <Form.Label>Month</Form.Label>
                <Form.Control as="select" onChange={e => this.setState({ month: e.target.value })} defaultValue={this.props.rowData.month} placeholder="Select Month" >
                  <option>January</option>
                  <option>February</option>
                  <option>March</option>
                  <option>April</option>
                  <option>May</option>
                  <option>June</option>
                  <option>July</option>
                  <option>August</option>
                  <option>September</option>
                  <option>October</option>
                  <option>November</option>
                  <option>December</option>
                </Form.Control>
              </Form.Group>
              <Form.Group controlId="amountClaimed">
                <Form.Label>&#8377;Amount</Form.Label>
                <Form.Control type="number" onChange={e => this.setState({ amountClaimed: e.target.value })} defaultValue={this.props.rowData.amountClaimed} placeholder="Enter Amount" />
              </Form.Group>
              <Form.Group controlId="status">
                <Form.Label>Status</Form.Label>
                <Form.Control as="select" onChange={e => this.setState({ status: e.target.value })} defaultValue={this.props.rowData.status} placeholder="Select status" >
                  <option>Open</option>
                  <option>Approved</option>
                  <option>Pending</option>
                  <option>Rejected</option>

                </Form.Control>
              </Form.Group>
              <Form.File id="formcheck-api-custom" custom>
                <Form.File.Input isValid />
                <Form.File.Label data-browse="Browse">
                  Select File(s)
              </Form.File.Label>
                <Form.Control.Feedback type="valid"></Form.Control.Feedback>
              </Form.File>

              <Form.Group controlId="comments">
                <Form.Label>Comments</Form.Label>
                <Form.Control type="textarea" onChange={e => this.setState({ comments: e.target.value })} placeholder="Enter comments..." />
              </Form.Group>

            </Modal.Body>
            <Modal.Footer>
              <Button variant="secondary" onClick={() => this.handleModalShowHide()}>
                Close
                    </Button>
              <Button variant="primary" onClick={this.handleSubmit}>
                Save Changes
                    </Button>
            </Modal.Footer>
            </Form>
          </Modal>
        
      </div >
    )
  }

}

export default EditModalDialog;