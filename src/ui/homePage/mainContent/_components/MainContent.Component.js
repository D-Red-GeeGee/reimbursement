import React, { Component } from 'react';
import ReimbursementList from './ReimbursementList.Component.js'
import { MainContentServices } from '../_services/mainContent.services.js';
import store from "../../../../data_store/index.js";
import ButtonCellRenderer from './BtnCellRenderer.Component.js';

class MainContent extends React.Component {

   constructor(props) {
      super(props);
      store.dispatch( updateReimbursementGridOptions({
         columnDefs: [
             { headerName: "Amount Claimed", field: "amountClaimed" },
             { headerName: "Status", field: "status" },
             { headerName: "Month", field: "month" },
             { headerName: "Documents", field: "documents" },
             {
                 headerName: "Actions", field: "actions", pinned: "right",
                 cellRenderer: 'btnCellRenderer',
             }
         ],
         frameworkComponents: {
             btnCellRenderer: ButtonCellRenderer,
         }
     }));
   } 
   componentDidMount(){
      MainContentServices.getReimbursements()
      .then(
         reimbursements => {
            console.log("fetching reimbursement data...");
            store.dispatch( updateReimbursements(reimbursements));
         },
         error => console.log("Error while fetching data.")
      );
   } 
   
   render() {
      return (
         <ReimbursementList />
      );
   }
}
export default MainContent;