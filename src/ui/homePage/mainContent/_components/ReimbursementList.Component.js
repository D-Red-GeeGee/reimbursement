import React from "react";
import { connect } from "react-redux";
import { Accordion } from 'react-bootstrap';
import { Card } from 'react-bootstrap';
import { AgGridReact } from 'ag-grid-react';
import AddModalDialog from './AddModalDlg.Component.js';


const mapStateToProps = state => {
    return {
        reimbursements: state.reimbursements,
        grid: state.reimbursementGridOptions
    };
};

const ConnectedList = ({ reimbursements, grid }) => (
    <div className="container">
        <div className="row">
            <Accordion className="col-md-12">
                {
                    reimbursements.map((element, index) => (

                        <Card>
                            <Card.Header>
                                <Accordion.Toggle variant="secondary" as={Card.Header} eventKey={index.toString()} style={{ width: "100%" }}>
                                    <div className="container" >
                                        <div className="row">
                                         <div className="col"><span>{element.type.toUpperCase()}</span> <span className="badge badge-light">{element.totalClaims}</span></div>
                                        </div>
                                    </div>
                                </Accordion.Toggle>
                            </Card.Header>
                            <Accordion.Collapse eventKey={index.toString()}>
                                <Card.Body>
                                    <div className="float-right"><AddModalDialog reimType={element._id}/></div>
                                    <div className="ag-theme-alpine" style={{ height: '500px', width: '100%' }}>
                                        {element.claimDetails._id}
                                        <AgGridReact
                                            columnDefs={grid.columnDefs}
                                            rowData={element.claimDetails}
                                            frameworkComponents={grid.frameworkComponents}>
                                        </AgGridReact>
                                    </div>
                                </Card.Body>
                            </Accordion.Collapse>
                        </Card>

                    ))

                }
            </Accordion>
        </div>
    </div>
);

const ReimbursementList = connect(mapStateToProps)(ConnectedList);

export default ReimbursementList;