import React, { Component } from 'react';
import EditModalDialog from './EditModalDlg.Component.js';
import { MainContentServices } from '../_services/mainContent.services.js';
import store from "../../../../data_store/index.js";
class ButtonCellRenderer extends Component {
  constructor(props) {
    super(props);
    this.btnClickedHandler = this.btnClickedHandler.bind(this);
  }
  btnClickedHandler(e) {
    
    MainContentServices.removeReimbursement(this.props.data)
      .then(
        result => {
          MainContentServices.getReimbursements()
            .then(
              reimbursements => {
                store.dispatch(updateReimbursements(reimbursements));
              },
              error => console.log("Error while fetching data.")
            );

        },
        error => console.log("Error while updating data.")
      );
  }
  render() {
    const rowdata = this.props.data;
    return (
      <div className="container">
        <div className="row">
          <div className="col">
            <EditModalDialog rowData={rowdata} />
          </div>
          <div className="col">
            <button id="remove_row" value="remove" className="btn btn-danger" onClick={this.btnClickedHandler}>Remove</button>
          </div>
        </div>


      </div>

    )
  }
}
export default ButtonCellRenderer;