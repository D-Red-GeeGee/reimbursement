export function configureFakeBackend() {
    
    let users = [{ id: 1, username: 'test', password: 'test', firstName: 'Test', lastName: 'User' }];

    let reimbursements = {
        "reimbursementDetails": [
            {
                "type":"reimbursementType1",
                "totalClaim": 100,
                "claims": [
                    {
                        "documents": ["passport.jpg", "addressProf.jpg"],
                        "amountClaimed": 0,
                        "status": "Pending",
                        "month": "July"
                    },
                    {
                        "documents": [],
                        "amountClaimed": 100,
                        "status": "Approved",
                        "month": "June"
                    }
                ]
            },
            {
                "type":"reimbursementType2",
                "totalClaim": 5000,
                "claims": [
                    {
                        "documents": [],
                        "amountClaimed": 1000,
                        "status": "Rejected",
                        "month": "August"
                    },
                    {
                        "documents": [],
                        "amountClaimed": 5000,
                        "status": "Approved",
                        "month": "April"
                    }
                ]
            }
        ]
    }

    let realFetch = window.fetch;
    window.fetch = function (url, opts) {
        return new Promise((resolve, reject) => {
            setTimeout(() => {

                 if (url.endsWith('/users/authenticate') && opts.method === 'POST') {
                    let params = JSON.parse(opts.body);

                    let filteredUsers = users.filter(user => {
                        return user.username === params.username && user.password === params.password;
                    });

                    if (filteredUsers.length) {
                        let user = filteredUsers[0];
                        let responseJson = {
                            id: user.id,
                            username: user.username,
                            firstName: user.firstName,
                            lastName: user.lastName
                        };
                        resolve({ ok: true, text: () => Promise.resolve(JSON.stringify(responseJson)) });
                    } else {
                        reject('Username or password is incorrect');
                    }

                    return;
                } 

                if (url.endsWith('/users') && opts.method === 'GET') {
                    if (opts.headers && opts.headers.Authorization === `Basic ${window.btoa('test:test')}`) {
                        resolve({ ok: true, text: () => Promise.resolve(JSON.stringify(users)) });
                    } else {
                        resolve({ status: 401, text: () => Promise.resolve() });
                    }

                    return;
                }
                
                 if (url.endsWith('/reimbursements') && opts.method === 'GET') {
                    if (opts.headers && opts.headers.Authorization === `Basic ${window.btoa('test:test')}`) {
                        resolve({ ok: true, text: () => Promise.resolve(JSON.stringify(reimbursements)) });
                    } else {
                        resolve({ status: 401, text: () => Promise.resolve() });
                    }

                    return;
                } 


                realFetch(url, opts).then(response => resolve(response));

            }, 500);
        });
    }
}