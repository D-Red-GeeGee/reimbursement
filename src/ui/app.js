import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { PrivateRoute } from './_components/PrivateRoute.js';
import { HomePage } from './homePage/HomePage.js';
import { LoginPage } from './loginPage/LoginPage.js';

class App extends Component {
   render() {
      return (
            <div className="container">
               <div className="col">
                  <Router>
                     <div className="containner">
                        <PrivateRoute exact path="/" component={HomePage} />
                        <Route path="/login" component={LoginPage} />
                     </div>
                  </Router>
               </div>
            </div>
           
      );
   }
}
export default App;