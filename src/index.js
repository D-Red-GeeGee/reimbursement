import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from "react-redux";
import store from "./data_store/index";
import App from './ui/app.js';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-alpine.css';

/* import { configureFakeBackend } from './ui/_helpers/fake-backend.js';
configureFakeBackend(); */
ReactDOM.render(<Provider store={store}><App /></Provider>, document.getElementById('app'));



