import { ADD_REIMBURSEMENTS } from "../constants/action-types";
import { UPDATE_REIMBURSEMENTS } from "../constants/action-types";
import { UPDATE_REIMBURSEMENT_GRID_OPTIONS } from "../constants/action-types";

export function addReimbursements(payload) {
    return { type: ADD_REIMBURSEMENTS, payload }
}
export function updateReimbursementGridOptions(payload) {
  return { type: UPDATE_REIMBURSEMENT_GRID_OPTIONS, payload }
}
export function updateReimbursements(payload) {
  return { type: UPDATE_REIMBURSEMENTS, payload }
}