import { ADD_REIMBURSEMENTS } from "../constants/action-types";
import { UPDATE_REIMBURSEMENTS } from "../constants/action-types";
import { UPDATE_REIMBURSEMENT_GRID_OPTIONS } from "../constants/action-types";

const initialState = {
    reimbursements: [],
    reimbursementGridOptions: {}
};

function rootReducer(state = initialState, action) {
    if (action.type === ADD_REIMBURSEMENTS) {
        return Object.assign({}, state, {
            reimbursements: state.reimbursements.concat(action.payload)
        });
    }
    if (action.type === UPDATE_REIMBURSEMENTS) {
        return Object.assign({}, state, {
            reimbursements: action.payload
        });
    }
    if (action.type === UPDATE_REIMBURSEMENT_GRID_OPTIONS) {
        return Object.assign({}, state, {
            reimbursementGridOptions: action.payload
        });
    }
    return state;
};

export default rootReducer;