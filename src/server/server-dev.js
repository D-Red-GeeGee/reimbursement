import path from 'path'
import express from 'express'
import webpack from 'webpack'
import webpackDevMiddleware from 'webpack-dev-middleware'
import webpackHotMiddleware from 'webpack-hot-middleware'
import config from '../../webpack.dev.config.js'
import BodyParser from "body-parser";
import mongodb from "mongodb";

const ObjectId = mongodb.ObjectID;
const MongoClient = mongodb.MongoClient
const CONNECTION_URL = "mongodb+srv://GeeGee4u:Geegee@2019@clustergee4u.haaej.mongodb.net/test?retryWrites=true&w=majority";
const DATABASE_NAME = "test";

const app = express(),
  DIST_DIR = __dirname,
  HTML_FILE = path.join(DIST_DIR, 'index.html'),
  compiler = webpack(config)

app.use(BodyParser.json());
app.use(BodyParser.urlencoded({ extended: true }));
app.use(express.static(DIST_DIR));

app.use(webpackDevMiddleware(compiler, {
  publicPath: config.output.publicPath
}))

app.use(webpackHotMiddleware(compiler))
/* app.get('*', (req, res, next) => {
  compiler.outputFileSystem.readFile(HTML_FILE, (err, result) => {
  if (err) {
    return next(err)
  }
  res.set('content-type', 'text/html')
  res.send(result)
  res.end()
  })
})  */

var database;

app.get("/users", (request, response) => {
  console.log("Getting users");
  database.collection("users").find({}).toArray((error, result) => {
    if (error) {
      return response.status(500).send(error);
    }
    response.send(result);
  });
});
app.post("/reimbursements/updateReimbursement", (request, response) => {
  console.log("Updating reimbursement..." + request.body._id);
  var myquery = { _id: new ObjectId(request.body._id) };
  delete request.body._id;
  var newvalues = { $set: request.body };
  database.collection("claims").updateOne(myquery, newvalues, function (error, result) {
    if (error) {
      return response.status(500).send(error);
    }
    response.send(result);
  });

});

app.post("/reimbursements/removeReimbursement", (request, response) => {
  console.log("Removing reimbursement..." + request.body._id);
  var myquery = { _id: new ObjectId(request.body._id) };
  database.collection("claims").deleteOne(myquery, function (error, result) {
    if (error) {
      return response.status(500).send(error);
    }
    database.collection('claims').aggregate([
      { '$project': { 'parent': new ObjectId(request.body.parent) } }
    ]).toArray(function (error, result) {
      if (error) {
        console.log(error);
      }
      console.log("Update Parent Ref: " + result);
      result = result.map(x => x._id)
      console.log("Mapping ids..");
      var myquery = { _id: new ObjectId(request.body.parent) };
      var newvalues = { $set: { claims: result } };
      database.collection('reimbursements').updateOne(myquery, newvalues, function (error, result) {
        if (error) {
          return response.status(500).send(error);
        }
        console.log("Parent Ref updated" + result.result);
        response.send(result);
      })
    });
  });

});


app.post("/reimbursements/addNewReimbursement", (request, response) => {
  console.log("Adding new claim..." + JSON.stringify(request.body));
  database.collection('claims').insertOne(request.body, (error, result) => {
    if (error) {
      return response.status(500).send(error);
    }
    console.log("claim Added on parent id: " + request.body.parent);

    database.collection('claims').aggregate([
      { '$project': { 'parent': new ObjectId(request.body.parent) } }
    ]).toArray(function (error, result) {
      if (error) {
        console.log(error);
      }
      console.log("Update Parent Ref: " + result);
      result = result.map(x => x._id)
      console.log("Mapping ids..");
      var myquery = { _id: new ObjectId(request.body.parent) };
      var newvalues = { $set: { claims: result } };
      database.collection('reimbursements').updateOne(myquery, newvalues, function (error, result) {
        if (error) {
          return response.status(500).send(error);
        }
        console.log("Parent Ref updated" + result.result);
        response.send(result);
      })
    });
  });

});

app.get("/reimbursements", (request, response) => {
  console.log("Getting reimbursements");
  database.collection("reimbursements").aggregate([
    // Unwind the source
    {
      $unwind: {
        path: "$claims",
        preserveNullAndEmptyArrays: true
      }
    },
    // Do the lookup matching
    {
      $lookup:
      {
        from: 'claims',
        localField: 'claims',
        foreignField: '_id',
        as: 'claimDetails'
      }
    },
    // Unwind the result arrays ( likely one or none )
    {
      $unwind: {
        path: "$claimDetails",
        preserveNullAndEmptyArrays: true
      }
    },
    // Group back to arrays
    {
      $group: {
        "id": { "$first": "$id" },
        "_id": "$_id",
        "type": { "$first": "$type" },
        "claims": { "$push": "$claims" },
        "claimDetails": { "$push": "$claimDetails" }
      },

    }, 
    {
      $sort: {
        'id': 1,
      }
    }
  ]).toArray(function (error, result) {
    if (error) {
      return response.status(500).send(error);
    }
    response.send(result);
  });
});

app.post("/users/authenticate", (request, response) => {
  console.log("Verifying user..." + request);
  database.collection("users").findOne({ "username": request.body.username, "password": request.body.password }, (error, result) => {
    if (error) {
      return response.status(500).send(error);
    }
    response.send(result);
  });
});

const PORT = process.env.PORT || 8001



app.listen(PORT, () => {
  console.log(`App listening to ${PORT}....`)
  console.log('Press Ctrl+C to quit.')
  MongoClient.connect(CONNECTION_URL, { useNewUrlParser: true }, (error, client) => {
    if (error) {
      throw error;
    }
    database = client.db(DATABASE_NAME);
    console.log("Connected to `" + DATABASE_NAME + "`!");
    console.log("html file: " + HTML_FILE);


  });
  console.log("outside connect");
});


